package com.guestlogix.app.search.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class Route(
    @Json(name = "airlineId") val airlineId: String,
    @Json(name = "origin") val origin: String,
    @Json(name = "destination") val destination: String
)
