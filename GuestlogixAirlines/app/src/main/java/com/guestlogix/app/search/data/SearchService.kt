package com.guestlogix.app.search.data

import com.guestlogix.app.search.model.Airline
import com.guestlogix.app.search.model.Airport
import com.guestlogix.app.search.model.Route
import io.reactivex.Single
import retrofit2.http.GET

interface SearchService {

    // https://gitlab.com/candidosg/guestlogix-app/raw/master/data/airlines.json
    @GET("airlines.min.json")
    fun getAirlines(): Single<List<Airline>>

    @GET("airports.min.json")
    fun getAirports(): Single<List<Airport>>

    @GET("routes.min.json")
    fun getRoutes(): Single<List<Route>>

}