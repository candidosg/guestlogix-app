package com.guestlogix.app.search.model

import com.google.android.gms.maps.model.LatLng
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Airport(
    @Json(name = "name") val name: String = "",
    @Json(name = "city") val city: String = "",
    @Json(name = "country") val country: String = "",
    @Json(name = "iata3") val iata3: String = "",
    @Json(name = "latitude") val latitude: Double = 0.0,
    @Json(name = "longitude") val longitude: Double = 0.0
) {
    fun getLatLng(): LatLng {
        return LatLng(this.latitude, this.longitude)
    }

    fun iataIsValid(): Boolean {
        return this.iata3.trim() != "\\N"
    }
}