package com.guestlogix.app.search.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.guestlogix.app.base.exception.*
import com.guestlogix.app.base.presentation.ViewActionState
import com.guestlogix.app.base.presentation.ViewError
import com.guestlogix.app.search.data.SearchRepository
import com.guestlogix.app.search.domain.GraphImpl
import com.guestlogix.app.search.domain.GraphRoutes
import com.guestlogix.app.search.model.Airline
import com.guestlogix.app.search.model.Airport
import com.guestlogix.app.search.model.Route
import io.reactivex.disposables.Disposable

class SearchViewModel constructor(
    private val searchRepository: SearchRepository
) : ViewModel() {

    private val airlineLiveData: MutableLiveData<ViewActionState<List<Airline>>> = MutableLiveData()
    private val airportLiveData: MutableLiveData<ViewActionState<List<Airport>>> = MutableLiveData()
    private val routeLiveData: MutableLiveData<ViewActionState<List<Route>>> = MutableLiveData()

    private var disposable: Disposable? = null

    override fun onCleared() {
        disposable?.dispose()
        super.onCleared()
    }

    fun getAirlines(): LiveData<ViewActionState<List<Airline>>> {
        return airlineLiveData
    }

    fun fetchAirlines() {
        airlineLiveData.postValue(ViewActionState.loading())
        disposable = searchRepository.getAirlines()
            .subscribe({
                airlineLiveData.postValue(ViewActionState.complete(it))
            }, {
                handleFailure(it, airlineLiveData)
            })
    }

    fun getAirports(): LiveData<ViewActionState<List<Airport>>> {
        return airportLiveData
    }

    fun fetchRoutes() {
        routeLiveData.postValue(ViewActionState.loading())
        disposable = searchRepository.getRoutes()
            .subscribe({
                routeLiveData.postValue(ViewActionState.complete(it))
            }, {
                handleFailure(it, routeLiveData)
            })
    }

    fun getRoutes(): LiveData<ViewActionState<List<Route>>> {
        return routeLiveData
    }

    fun fetchAirports() {
        airportLiveData.postValue(ViewActionState.loading())
        disposable = searchRepository.getAirports()
            .subscribe({
                airportLiveData.postValue(ViewActionState.complete(it))
            }, {
                handleFailure(it, airportLiveData)
            })
    }

    fun graphRoutes(routes: List<Route>, airports: List<Airport>): GraphRoutes {
        return GraphRoutes(
            routes = routes,
            airports = airports,
            graph = GraphImpl(directed = true, defaultCost = 1)
        )
    }

    protected fun <T> handleFailure(
        ex: Throwable,
        liveData: MutableLiveData<ViewActionState<T>>
    ) {
        val error = when (ex) {
            NetworkError, is UnknownBaseHostError -> ViewError.NoInternetConnection
            UnauthorizedError -> ViewError.UnauthorizedUser
            is HttpError -> ViewError.NoMappedHTTPCode
            is BusinessException -> ViewError.BusinessValidation(ex)
            else -> ViewError.NoMappedError
        }
        liveData.postValue(ViewActionState.failure(error))
    }
}