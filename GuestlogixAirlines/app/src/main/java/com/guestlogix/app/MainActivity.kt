package com.guestlogix.app

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.guestlogix.app.base.presentation.ViewActionState
import com.guestlogix.app.search.model.Airline
import com.guestlogix.app.search.model.Airport
import com.guestlogix.app.search.model.Route
import com.guestlogix.app.search.presentation.SearchViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import kotlinx.android.synthetic.main.activity_main.*
import com.google.android.gms.maps.model.LatLngBounds
import com.guestlogix.app.search.domain.GraphRoutes
import com.guestlogix.app.search.model.StopOver


class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    private val searchViewModel: SearchViewModel by viewModel()

    private lateinit var map: GoogleMap

    private lateinit var airports: List<Airport>
    private lateinit var airlines: List<Airline>
    private lateinit var routes: List<Route>
    private lateinit var graphRoutes: GraphRoutes

    private var originAirport: Airport? = null
    private var destinationAirport: Airport? = null

    private val TORONTO = LatLng(43.6570403,-79.4609332)
    private val ZOOM_LEVEL = 8f
    private val STROKE_WIDTH_PX = 8

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getRoutes()
        getAirlines()
        getAirports()
        startMap()
    }

    private fun startMap() {
        val mapFragment : SupportMapFragment? = supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        map = googleMap ?: return
        with(googleMap) {
            moveCamera(CameraUpdateFactory.newLatLngZoom(TORONTO, ZOOM_LEVEL))
            addMarker(MarkerOptions().position(TORONTO))
        }
    }

    private fun getAirlines() {
        searchViewModel.getAirlines().observe(this,
            Observer<ViewActionState<List<Airline>>> {
                if (it != null) this.handleDataStateAirline(it) })
        searchViewModel.fetchAirlines()
    }

    private fun getAirports() {
        searchViewModel.getAirports().observe(this,
            Observer<ViewActionState<List<Airport>>> {
                if (it != null) this.handleDataStateAirport(it) })
        searchViewModel.fetchAirports()
    }

    private fun getRoutes() {
        searchViewModel.getRoutes().observe(this,
            Observer<ViewActionState<List<Route>>> {
                if (it != null) this.handleDataStateRoutes(it) })
        searchViewModel.fetchRoutes()
    }

    private fun handleDataStateAirline(viewState: ViewActionState<List<Airline>>) {
        when (viewState) {
            is ViewActionState.Loading -> setupScreenForLoadingState(View.VISIBLE)
            is ViewActionState.Complete -> {
                setupScreenForLoadingState(View.GONE)
                airlines = viewState.result
            }
            is ViewActionState.Error -> setupScreenForError()
        }
    }

    private fun handleDataStateAirport(viewState: ViewActionState<List<Airport>>) {
        when (viewState) {
            is ViewActionState.Loading -> setupScreenForLoadingState(View.VISIBLE)
            is ViewActionState.Complete -> {
                setupScreenForLoadingState(View.GONE)
                airports = viewState.result
                prepareAutoComplete()
                graphRoutes = searchViewModel.graphRoutes(routes, airports)
            }
            is ViewActionState.Error -> setupScreenForError()
        }
    }

    private fun handleDataStateRoutes(viewState: ViewActionState<List<Route>>) {
        when (viewState) {
            is ViewActionState.Loading -> setupScreenForLoadingState(View.VISIBLE)
            is ViewActionState.Complete -> {
                setupScreenForLoadingState(View.GONE)
                routes = viewState.result
            }
            is ViewActionState.Error -> setupScreenForError()
        }
    }

    private fun prepareAutoComplete() {
        val cities =  arrayListOf<String>()

        if (airports.isNotEmpty()) {
            airports.forEach {
                if (it.iataIsValid()) {
                    cities.add("${it.city} - ${it.iata3}")
                }
            }
        }

        if (cities.isNotEmpty()) {
            val adapter = ArrayAdapter<String>(
                this,
                android.R.layout.simple_dropdown_item_1line, cities
            )

            autoCompleteOrigin.threshold = 2
            autoCompleteOrigin.setAdapter(adapter)
            autoCompleteOrigin.onItemClickListener = AdapterView.OnItemClickListener{ parent, _, position, _ ->
                hideKeyboard()
                autoCompleteDestination.requestFocus()

                val airportSelected = parent.getItemAtPosition(position) as String
                val iataCode = airportSelected.split(" - ")[1]
                originAirport = graphRoutes.fetchAirport(iataCode)

                if (originAirport != null) {
                    searchRoutes()
                }
            }

            autoCompleteDestination.threshold = 3
            autoCompleteDestination.setAdapter(adapter)
            autoCompleteDestination.onItemClickListener = AdapterView.OnItemClickListener{ parent, _, position, _ ->
                hideKeyboard()

                val airportSelected = parent.getItemAtPosition(position) as String
                val iataCode = airportSelected.split(" - ")[1]
                destinationAirport = graphRoutes.fetchAirport(iataCode)

                if (destinationAirport != null) {
                    searchRoutes()
                }
            }
        }
    }

    private fun searchRoutes() {
        if (originAirport != null && destinationAirport != null) {
            val stopsOver = graphRoutes.shortPath(originAirport!!.iata3, destinationAirport!!.iata3)

            if (stopsOver.isNotEmpty()) {
                drawRoute(stopsOver)
            } else {
                Toast
                    .makeText(
                        applicationContext,
                        R.string.sorry_not_found_routes,
                        Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    private fun drawRoute(stopsOver: List<StopOver>) {
        if (stopsOver.isNotEmpty()) {
            map.clear()
            with(map) {

                uiSettings.isZoomControlsEnabled = true

                val options =
                    PolylineOptions().width(STROKE_WIDTH_PX.toFloat()).color(Color.BLUE)
                        .geodesic(true)

                val builder = LatLngBounds.Builder()

                stopsOver.forEach {
                    options.add(it.origin.getLatLng(), it.destination.getLatLng())

                    builder.include(it.origin.getLatLng())
                    builder.include(it.destination.getLatLng())
                }

                addPolyline(options)

                // Move the googleMap so that it is centered on the mutable polyline.
                val bounds = builder.build()
                animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 150))


                val lastDestination = stopsOver.last().destination
                addMarker(MarkerOptions()
                    .position(lastDestination.getLatLng())
                    .title(lastDestination.city)
                    .snippet(lastDestination.name))
            }
        }
    }

    private fun setupScreenForLoadingState(visibility: Int) {
        loading.visibility = visibility
    }

    private fun setupScreenForError() {
        Toast
            .makeText(
                applicationContext,
                R.string.sorry_we_cant_load_routes,
                Toast.LENGTH_LONG)
            .show()
    }

    private fun hideKeyboard() {
        val view = this.currentFocus
        view?.let { v ->
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.let { it.hideSoftInputFromWindow(v.windowToken, 0) }
        }
    }
}
