package com.guestlogix.app.search.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class StopOver(
    @Json(name = "origin") val origin: Airport,
    @Json(name = "destination") val destination: Airport
)