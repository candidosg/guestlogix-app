//package com.guestlogix.app.base.data
//
//import com.guestlogix.app.base.exception.NetworkError
//import com.guestlogix.app.base.extensions.performOnBack
//import com.guestlogix.app.base.network.NetworkHandler
//import io.reactivex.Single
//
//class BaseRemoteRepository constructor(
//    private val networkHandler: NetworkHandler
//) {
//    /**
//     * Function to check if there is internet connection
//     *
//     * @param body Another function to execute when there is internet connection
//     * @return An [Completable] with [NetworkError] when there is no internet
//     * connection or with the body function result
//     */
//    fun <T> checkConnection(
//        body: () -> Single<T>
//    ): Single<T> {
//        return when (networkHandler.isConnected) {
//            false -> Single.error(NetworkError)
//            true -> body()
//                .performOnBack()
//        }
//    }
//}