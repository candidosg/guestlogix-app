package com.guestlogix.app.di

import androidx.lifecycle.ViewModelProvider
import com.guestlogix.app.search.data.SearchRepository
import com.guestlogix.app.search.presentation.SearchViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    factory { ViewModelProvider.NewInstanceFactory() as ViewModelProvider.Factory}
    factory { SearchRepository(get()) }
    viewModel { SearchViewModel(get()) }
}