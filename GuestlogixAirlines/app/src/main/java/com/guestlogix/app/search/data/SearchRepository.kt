package com.guestlogix.app.search.data

//import com.guestlogix.app.base.data.BaseRemoteRepository
import com.guestlogix.app.search.model.Airline
import com.guestlogix.app.search.model.Airport
import com.guestlogix.app.search.model.Route
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SearchRepository(
    private val searchService: SearchService
) {

    fun getAirlines(): Single<List<Airline>> {
        return searchService.getAirlines()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }

    fun getAirports(): Single<List<Airport>> {
        return searchService.getAirports()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }

    fun getRoutes(): Single<List<Route>> {
        return searchService.getRoutes()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
    }
}