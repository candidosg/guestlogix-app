package com.guestlogix.app.search.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Airline(
    @Json(name = "name") val name: String,
    @Json(name = "2_digit_code") val twoDigitCode: String,
    @Json(name = "3_digit_code") val threeDigitCode: String,
    @Json(name = "country") val country: String
)