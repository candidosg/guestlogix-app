package com.guestlogix.app

import android.app.Application
import com.guestlogix.app.di.appModule
import com.guestlogix.app.di.networkModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

open class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        setupTimber()

        startKoin {
            androidContext(this@MainApplication)
            modules(appModule, networkModule)
        }

    }

    private fun setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

}