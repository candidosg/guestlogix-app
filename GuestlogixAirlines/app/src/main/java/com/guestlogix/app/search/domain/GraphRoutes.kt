package com.guestlogix.app.search.domain

import android.location.Location
import com.guestlogix.app.search.model.Airport
import com.guestlogix.app.search.model.Route
import com.guestlogix.app.search.model.StopOver
import timber.log.Timber

class GraphRoutes(
    private val routes: List<Route>,
    private val airports: List<Airport>,
    private var mapAirports: MutableMap<String, Airport> = mutableMapOf(),
    private val graph: GraphImpl<String, Int>
) {
    init {
        generateMapAirports()
        addRoutesInGraph()
    }

    private fun addRoutesInGraph() {
        routes.forEach {
            try {
                val distance = calculateDistance(it.origin, it.destination)
                graph.addArc(it.origin to it.destination, distance)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }

    @Throws(Exception::class)
    private fun calculateDistance(origin: String, destination: String): Int {
        val airportOrigin = fetchAirport(origin)
        val airportDestination = fetchAirport(destination)

        if (airportOrigin == null || airportDestination == null) {
            throw Exception("There is no airport with valid")
        }

        val originPoint = Location("origin")
        originPoint.latitude = airportOrigin.latitude
        originPoint.longitude = airportOrigin.longitude

        val destinationPoint = Location("destination")

        destinationPoint.latitude = airportDestination.latitude
        destinationPoint.longitude = airportDestination.longitude

        return originPoint.distanceTo(destinationPoint).toInt()
    }


    fun fetchAirport(iata: String): Airport? {
        return mapAirports[iata.trim()]
    }

    fun shortPath(origin: String, destination: String): List<StopOver> {
        val stopsOver = arrayListOf<StopOver>()
        val (path, value) =  shortestPath(graph, origin, destination)
        Timber.d("path = $path, value = $value")
        for (i in path.indices) {
            if (i+1 < path.size) {
                stopsOver.add(
                    StopOver(
                        origin = fetchAirport(path[i])!!,
                        destination = fetchAirport(path[i+1])!!
                    )
                )
            }
        }
        return stopsOver
    }

    private fun generateMapAirports() {
        mapAirports = mutableMapOf()
        airports.forEach {
            if (it.iataIsValid()) {
                mapAirports[it.iata3.trim()] = it
            }
        }
    }
}